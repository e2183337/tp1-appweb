import { createRouter, createWebHistory } from "vue-router";

const routes= [
    {path:"/", name: "LoginView", component: () => import("../views/LoginView.vue")},
    {path:"/signup", name: "SignupView", component: () => import("../views/SignupView.vue")},
    {path:"/chat", name: "ChatView", component: () => import("../views/ChatView.vue")},
    {path:"/profil", name: "ProfilView", component: () => import("../views/ProfilView.vue")},

]

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
});
  
export default router;