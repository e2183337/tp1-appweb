import { createApp, markRaw } from 'vue'
import { createPinia } from "pinia"
import piniaPluginPersistedState from "pinia-plugin-persistedstate"
import { useLocalStorage } from '@vueuse/core'
import App from './App.vue'
import router from './router/router'
import "../node_modules/bulma/css/bulma.css";

const pinia = createPinia();

const app = createApp(App);


pinia.use(piniaPluginPersistedState);

pinia.use(({ store }) => {
    store.router = markRaw(router);
});

app.use(pinia);

app.use(router);

app.mount("#app");
