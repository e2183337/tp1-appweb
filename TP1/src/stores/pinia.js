import { defineStore } from "pinia";

import { initializeApp } from "firebase/app";
import { useStorage } from "@vueuse/core";

export const Store = defineStore("Store", {
  state() {
    return {
    token:useStorage("token",null),
  }
  },
  actions: {
    recupererToken() {
      console.log(sessionStorage.getItem("token"))
      if (sessionStorage.getItem("token")) {
        this.token = sessionStorage.getItem("token");
      } else {
        console.log("session storage is empty")
      }
      console.log(this.token)
  },
    app() {
      const firebaseConfig = {
        apiKey: "AIzaSyC7NbMd1AVAgWp1-gC_oll8leBZjf1UttU",
        authDomain: "fir-1-11707.firebaseapp.com",
        projectId: "fir-1-11707",
        storageBucket: "fir-1-11707.appspot.com",
        messagingSenderId: "415765394036",
        appId: "1:415765394036:web:cc0d1e1f3ddc9a0c4d1721"
        };

    const app = initializeApp(firebaseConfig);

    return app;
    },

  },
  getters: {

  },
  persist:true,
});
